package factoryPackage;

public class TicketFactory {
	public static Ticket ticketType(String value){
		Ticket ticket = null;
		switch (value){
		case "galeria":
			ticket = new Galeria();
			break;
			case "cancha":
			ticket = new Cancha();
			break;
			case "vip":
			ticket = new Vip();
			break;
		}
		return ticket;
	}
}
