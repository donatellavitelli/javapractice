package factoryPackage;

public class Main {

	public static void main(String[] args) {
		Ticket ticket1 = TicketFactory.ticketType("galeria");
		System.out.println(ticket1.toString());
		Ticket ticket2 = TicketFactory.ticketType("cancha");
		System.out.println(ticket2.toString());
		Ticket ticket3 = TicketFactory.ticketType("vip");
		System.out.println(ticket3.toString());
	}

}
