package factoryPackage;

public class Vip extends Ticket{
	@Override
	public float getPrice(){
		return 100;
	}

	@Override
	public String getDescription() {
		return "acceso al salon vip, meet & greet con la banda, sticker de regalo";
	}

}
