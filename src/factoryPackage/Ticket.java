package factoryPackage;

public abstract class Ticket {
	
	public abstract float getPrice();
	public abstract String getDescription();
	
	@Override
	public String toString() {
		return "Price =" + this.getPrice() + "	description = " + this.getDescription();
	}
	
}
