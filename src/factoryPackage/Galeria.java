package factoryPackage;

public class Galeria extends Ticket{
	
	@Override
	public float getPrice(){
		return 20;
	}

	@Override
	public String getDescription() {
		return "solo acceso a galeria";
	}
}
